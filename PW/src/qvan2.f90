!
! Copyright (C) 2001-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
subroutine qvan2 (ngy, ih, jh, np, qmod, qg, ylmk0, on_device)
  !-----------------------------------------------------------------------
  !
  !    This routine computes the fourier transform of the Q functions
  !    The interpolation table for the radial fourier transform is stored 
  !    in qrad.
  !
  !    The formula implemented here is
  !
  !     q(g,i,j) = sum_lm (-i)^l ap(lm,i,j) yr_lm(g^) qrad(g,l,i,j)
  !
  !
  USE kinds, ONLY: DP
  USE us, ONLY: dq, qrad
  USE uspp_param, ONLY: lmaxq, nbetam
  USE uspp, ONLY: nlx, lpl, lpx, ap, indv, nhtolm
  implicit none
  !
  ! Input variables
  !
  integer,intent(IN) :: ngy, ih, jh, np
  ! ngy   :   number of G vectors to compute
  ! ih, jh:   first and second index of Q
  ! np    :   index of pseudopotentials
  !
  real(DP),intent(IN) :: ylmk0 (ngy, lmaxq * lmaxq), qmod (ngy)
  ! ylmk0 :  spherical harmonics
  ! qmod  :  moduli of the q+g vectors
  !
  ! output: the fourier transform of interest
  !
  real(DP),intent(OUT) :: qg (2,ngy)
  !
  !     here the local variables
  !
  real (DP) :: sig
  ! the nonzero real or imaginary part of (-i)^L 
  real (DP), parameter :: sixth = 1.d0 / 6.d0
  !
  integer :: nb, mb, ijv, ivl, jvl, ig, lp, l, lm, i0, i1, i2, i3, ind
  ! nb,mb  : atomic index corresponding to ih,jh
  ! ijv    : combined index (nb,mb)
  ! ivl,jvl: combined LM index corresponding to ih,jh
  ! ig     : counter on g vectors
  ! lp     : combined LM index
  ! l-1    is the angular momentum L
  ! lm     : all possible LM's compatible with ih,jh
  ! i0-i3  : counters for interpolation table
  ! ind    : ind=1 if the results is real (l even), ind=2 if complex (l odd)
  !
  real(DP) :: dqi, qm, px, ux, vx, wx, uvx, pwx, work, qm1
  ! 1 divided dq
  ! qmod/dq
  ! measures for interpolation table
  ! auxiliary variables for intepolation
  ! auxiliary variables
  !
  !     compute the indices which correspond to ih,jh
  !
  logical, intent(in) :: on_device
  !if (on_device) print *, "qvan2 on device!"
  dqi = 1.0_DP / dq
  nb = indv (ih, np)
  mb = indv (jh, np)
  if (nb.ge.mb) then
     ijv = nb * (nb - 1) / 2 + mb
  else
     ijv = mb * (mb - 1) / 2 + nb
  endif
  ivl = nhtolm(ih, np)
  jvl = nhtolm(jh, np)
  if (nb > nbetam .OR. mb > nbetam) &
       call errore (' qvan2 ', ' wrong dimensions (1)', MAX(nb,mb))
  if (ivl > nlx .OR. jvl > nlx) &
       call errore (' qvan2 ', ' wrong dimensions (2)', MAX(ivl,jvl))
  qg = 0.d0
  !
  !    and make the sum over the non zero LM
  !
  do lm = 1, lpx (ivl, jvl)
     lp = lpl (ivl, jvl, lm)
     if ( lp < 1 .or. lp > 49 ) call errore ('qvan2', ' lp wrong ', max(lp,1))
     !
     !     find angular momentum l corresponding to combined index lp
     !     (l is actually l+1 because this is the way qrad is stored, check init_us_1)
     !
     if (lp == 1) then
        l = 1
        sig = 1.0d0
        ind = 1
     elseif ( lp <= 4) then
        l = 2
        sig =-1.0d0
        ind = 2
     elseif ( lp <= 9 ) then
        l = 3
        sig =-1.0d0
        ind = 1
     elseif ( lp <= 16 ) then
        l = 4
        sig = 1.0d0
        ind = 2
     elseif ( lp <= 25 ) then
        l = 5
        sig = 1.0d0
        ind = 1
     elseif ( lp <= 36 ) then
        l = 6
        sig =-1.0d0
        ind = 2
     else
        l = 7
        sig =-1.0d0
        ind = 1
     endif
     sig = sig * ap (lp, ivl, jvl)

     qm1 = -1.0_dp !  any number smaller than qmod(1)

!$omp parallel do default(shared), private(qm,px,ux,vx,wx,i0,i1,i2,i3,uvx,pwx,work)
     do ig = 1, ngy
        !
        ! calculate quantites depending on the module of G only when needed
        !
#if ! defined _OPENMP
        IF ( ABS( qmod(ig) - qm1 ) > 1.0D-6 ) THEN
#endif
           !
           qm = qmod (ig) * dqi
           px = qm - int (qm)
           ux = 1.d0 - px
           vx = 2.d0 - px
           wx = 3.d0 - px
           i0 = INT( qm ) + 1
           i1 = i0 + 1
           i2 = i0 + 2
           i3 = i0 + 3
           uvx = ux * vx * sixth
           pwx = px * wx * 0.5d0
           work = qrad (i0, ijv, l, np) * uvx * wx + &
                  qrad (i1, ijv, l, np) * pwx * vx - &
                  qrad (i2, ijv, l, np) * pwx * ux + &
                  qrad (i3, ijv, l, np) * px * uvx
#if ! defined _OPENMP
           qm1 = qmod(ig)
        END IF
#endif
        qg (ind,ig) = qg (ind,ig) + sig * ylmk0 (ig, lp) * work
     enddo
!$omp end parallel do

  enddo

  return
end subroutine qvan2


module qvan2_gpu_m
contains

attributes(global) subroutine qvan2_kernel(ngy, ih, jh, np, qmod, qg, &
                                            ylmk0, lmaxq, nbetam, nlx, &
                                            qrad, lpl, lpx, ap, &
                                            nsp, lmaxx, lqmax, ijv, ivl, jvl )
  !-----------------------------------------------------------------------
  !
  !    This routine computes the fourier transform of the Q functions
  !    The interpolation table for the radial fourier trasform is stored 
  !    in qrad.
  !
  !    The formula implemented here is
  !
  !     q(g,i,j) = sum_lm (-i)^l ap(lm,i,j) yr_lm(g^) qrad(g,l,i,j)
  !
  !
  !USE kinds, ONLY: DP
  !USE us, ONLY: qrad=>qrad_d
  !USE uspp_param, ONLY: lmaxq=>lmaxq_d, nbetam=>nbetam_d
  !USE uspp, ONLY: lpl=>lpl_d, lpx=>lpx_d, ap=>ap_d, indv=>indv_d, nhtolm=>nhtolm_d
  implicit none
  INTEGER, PARAMETER :: DP = selected_real_kind(14,200)
  !
  ! Input variables
  !
  REAL(DP), PARAMETER:: dq = 0.01D0  !!!! BAD !!!
  integer,intent(IN) :: ngy, ih, jh, np, lmaxq, nbetam, nlx, nsp
  integer,intent(IN) :: lmaxx, lqmax, ijv, ivl, jvl
  attributes(value):: ngy, ih, jh, np, lmaxq, nbetam, nlx, nsp
  attributes(value):: lmaxx, lqmax, ijv, ivl, jvl
  ! ngy   :   number of G vectors to compute
  ! ih, jh:   first and second index of Q
  ! np    :   index of pseudopotentials
  !
  real(DP),intent(IN) :: ylmk0 (ngy, lmaxq * lmaxq), qmod (ngy)
  ! ylmk0 :  spherical harmonics
  ! qmod  :  moduli of the q+g vectors
  !
  ! From modules
  real(DP),intent(IN) :: qrad (ngy,nbetam*(nbetam+1)/2 , lmaxq, nsp)
  INTEGER ::             &! for each pair of combined momenta lm(1),lm(2): 
       lpx(nlx,nlx),     &! maximum combined angular momentum LM
       lpl(nlx,nlx,2*lqmax-1)    ! list of combined angular momenta  LM
  REAL(DP), intent(in) :: &
       ap(lqmax*lqmax,nlx,nlx)

  
  ! output: the fourier transform of interest
  !
  COMPLEX(DP),intent(OUT) :: qg (ngy)
  attributes(device):: ylmk0,qmod,qg,qrad,ap,indv,nhtolm
  !
  !     here the local variables
  !
  COMPLEX (DP) :: sig
  ! the nonzero real or imaginary part of (-i)^L 
  real (DP), parameter :: sixth = 1.d0 / 6.d0
  !
  integer :: nb, mb, ig, lp, l, lm, i0, i1, i2, i3
  real(DP) :: dqi, qm, px, ux, vx, wx, uvx, pwx, work, qm1
  real(DP) :: uvwx,pwvx,pwux,pxuvx
  ig= threadIdx%x+BlockDim%x*(BlockIdx%x-1)
  if (ig <= ngy) then
     !     compute the indices which correspond to ih,jh
     dqi = 1.0_DP / dq
     qg(ig) = 0.d0
     
     qm = qmod (ig) * dqi
     px = qm - int (qm)
     ux = 1.d0 - px
     vx = 2.d0 - px
     wx = 3.d0 - px
     i0 = INT( qm ) + 1
     i1 = i0 + 1
     i2 = i0 + 2
     i3 = i0 + 3
     uvx = ux * vx * sixth
     pwx = px * wx * 0.5d0
     
     do lm = 1, lpx (ivl, jvl)
        lp = lpl (ivl, jvl, lm)
         if (lp == 1) then
            l = 1
            sig = CMPLX(1.0d0, 0.0d0, kind=DP)
         elseif ( lp <= 4) then
            l = 2
            sig = CMPLX(0.d0, -1.0d0, kind=DP)
         elseif ( lp <= 9 ) then
            l = 3
            sig = CMPLX(-1.0d0, 0.d0, kind=DP)
         elseif ( lp <= 16 ) then
            l = 4
            sig = CMPLX(0.d0, 1.0d0, kind=DP)
         elseif ( lp <= 25 ) then
            l = 5
            sig = CMPLX(1.0d0, 0.d0, kind=DP)
         elseif ( lp <= 36 ) then
            l = 6
            sig = CMPLX(0.d0, -1.0d0, kind=DP)
         else
            l = 7
            sig = CMPLX(-1.0d0, 0.d0, kind=DP)
         endif
         !sig = sig * ap (lp, ivl, jvl)
              work = qrad (i0, ijv, l, np) * uvx * wx + &
                     qrad (i1, ijv, l, np) * pwx * vx - &
                     qrad (i2, ijv, l, np) * pwx * ux + &
                     qrad (i3, ijv, l, np) * px * uvx
           qg (ig) = qg (ig) + sig * CMPLX(ylmk0 (ig, lp) * work *  ap (lp, ivl, jvl), 0.d0, KIND=DP)
     end do

  end if

  return
end subroutine qvan2_kernel

subroutine qvan2_d(ngy, ih, jh, np, qmod, qg, ylmk0, &
                    qrad, lpx, lpl, ap, ijv, ivl, jvl )
  use cudafor
  use kinds, ONLY: DP
  use parameters, ONLY: lmaxx, lqmax
  use uspp_param, ONLY: lmaxq, nbetam, nh, nhm
  use uspp, ONLY: nlx
  USE ions_base,            ONLY : ntyp => nsp
  implicit none
  integer,intent(IN) :: ngy, ih, jh, np, ijv, ivl, jvl
  !
  real(DP),intent(IN) :: ylmk0 (ngy, lmaxq * lmaxq), qmod (ngy)
  !
  real(DP),intent(IN) :: qrad (ngy,nbetam*(nbetam+1)/2 , lmaxq, ntyp)
  INTEGER , intent(in)::  lpx(nlx,nlx), lpl(nlx,nlx,2*lqmax-1)
  REAL(DP),intent(in) :: ap(lqmax*lqmax,nlx,nlx)
  !
  COMPLEX(DP),device,intent(OUT) :: qg (ngy)
  attributes(device):: ylmk0, qmod, qrad, lpx, lpl, ap, indv, nhtolm

  type(dim3):: grid,tBlock

  tBlock = dim3(256,1,1)
  grid = dim3(ceiling(real(ngy)/tBlock%x),1,1)
  call qvan2_kernel<<<grid,tBlock>>>(ngy, ih, jh, np, qmod, qg, ylmk0, &
                                      lmaxq,nbetam,nlx, &
                                      qrad, lpl, lpx, ap, &
                                      ntyp, lmaxx, lqmax, ijv, ivl, jvl )
end subroutine qvan2_d

end module qvan2_gpu_m


!-------------------------------------------------------------------------
subroutine qvan2_acc (ngy, ih, jh, np, qmod, qg, ylmk0, on_device)
  !-----------------------------------------------------------------------
  !
  !    This routine computes the fourier transform of the Q functions
  !    The interpolation table for the radial fourier transform is stored 
  !    in qrad.
  !
  !    The formula implemented here is
  !
  !     q(g,i,j) = sum_lm (-i)^l ap(lm,i,j) yr_lm(g^) qrad(g,l,i,j)
  !
  !
  USE kinds, ONLY: DP
  USE us, ONLY: dq, qrad
  USE uspp_param, ONLY: lmaxq, nbetam
  USE uspp, ONLY: nlx, lpl, lpx, ap, indv, nhtolm
  USE qvan2_gpu_m, ONLY : qvan2_d
  implicit none
  !
  ! Input variables
  !
  integer,intent(IN) :: ngy, ih, jh, np
  ! ngy   :   number of G vectors to compute
  ! ih, jh:   first and second index of Q
  ! np    :   index of pseudopotentials
  !
  real(DP),intent(IN) :: ylmk0 (ngy, lmaxq * lmaxq), qmod (ngy)
  ! ylmk0 :  spherical harmonics
  ! qmod  :  moduli of the q+g vectors
  !
  ! output: the fourier transform of interest
  !
  complex(DP),intent(OUT) :: qg (ngy)
  !
  !     here the local variables
  !
  complex (DP) :: sig
  ! the nonzero real or imaginary part of (-i)^L 
  real (DP), parameter :: sixth = 1.d0 / 6.d0
  !
  integer :: nb, mb, ijv, ivl, jvl, ig, lp, l, lm, i0, i1, i2, i3
  ! nb,mb  : atomic index corresponding to ih,jh
  ! ijv    : combined index (nb,mb)
  ! ivl,jvl: combined LM index corresponding to ih,jh
  ! ig     : counter on g vectors
  ! lp     : combined LM index
  ! l-1    is the angular momentum L
  ! lm     : all possible LM's compatible with ih,jh
  ! i0-i3  : counters for interpolation table
  ! ind    : ind=1 if the results is real (l even), ind=2 if complex (l odd)
  !
  real(DP) :: dqi, qm, px, ux, vx, wx, uvx, pwx, work, qm1
  ! 1 divided dq
  ! qmod/dq
  ! measures for interpolation table
  ! auxiliary variables for intepolation
  ! auxiliary variables
  !
  !     compute the indices which correspond to ih,jh
  !
  logical, intent(in) :: on_device
  !if (on_device) print *, "qvan2_acc on device!"
  dqi = 1.0_DP / dq
  nb = indv (ih, np)
  mb = indv (jh, np)
  if (nb.ge.mb) then
     ijv = nb * (nb - 1) / 2 + mb
  else
     ijv = mb * (mb - 1) / 2 + nb
  endif
  ivl = nhtolm(ih, np)
  jvl = nhtolm(jh, np)
  if (nb > nbetam .OR. mb > nbetam) &
       call errore (' qvan2 ', ' wrong dimensions (1)', MAX(nb,mb))
  if (ivl > nlx .OR. jvl > nlx) &
       call errore (' qvan2 ', ' wrong dimensions (2)', MAX(ivl,jvl))
  
  !if (on_device) then
  !  !$acc update device(qrad, lpx, lpl, ap, indv, nhtolm)
  !  !$acc data present(qmod, qg, ylmk0, qrad, lpx, lpl, ap)
  !  !$acc host_data use_device(qmod, qg, ylmk0, qrad, lpx, lpl, ap)
  !  CALL qvan2_d(ngy, ih, jh, np, qmod, qg, ylmk0 , &
  !                qrad, lpx, lpl, ap, ijv, ivl, jvl)
  !  !$acc end host_data
  !  !$acc end data
  !  return 
  !  
  !end if
   
  !acc kernels present(qg) if(on_device)
  !qg = (0.d0, 0.d0)
  !acc end kernels
  !
  !    and make the sum over the non zero LM
  !
  !$acc kernels loop present(qmod(ngy), ylmk0(ngy, lmaxq * lmaxq), qg, qrad, lpx, lpl, ap) &
  !$acc num_workers(256) collapse(1) if(on_device)
  do ig = 1, ngy
     !
     ! calculate quantites depending on the module of G only when needed
     !
     !
     qg(ig) = (0.d0, 0.d0)
     qm = qmod (ig) * dqi
     px = qm - int (qm)
     ux = 1.d0 - px
     vx = 2.d0 - px
     wx = 3.d0 - px
     i0 = INT( qm ) + 1
     i1 = i0 + 1
     i2 = i0 + 2
     i3 = i0 + 3
     uvx = ux * vx * sixth
     pwx = px * wx * 0.5d0
     do lm = 1, lpx (ivl, jvl)
        lp = lpl (ivl, jvl, lm)
        !if ( lp < 1 .or. lp > 49 ) call errore ('qvan2', ' lp wrong ', max(lp,1))
        !
        !     find angular momentum l corresponding to combined index lp
        !     (l is actually l+1 because this is the way qrad is stored, check init_us_1)
        !
        if (lp == 1) then
           l = 1
           sig = CMPLX(1.0d0, 0.0d0, kind=DP)
        elseif ( lp <= 4) then
           l = 2
           sig = CMPLX(0.0d0, -1.0d0, kind=DP)
        elseif ( lp <= 9 ) then
           l = 3
           sig = CMPLX(-1.0d0, 0.0d0, kind=DP)
        elseif ( lp <= 16 ) then
           l = 4
           sig = CMPLX(0.0d0, 1.0d0, kind=DP)
        elseif ( lp <= 25 ) then
           l = 5
           sig = CMPLX(1.0d0, 0.0d0, kind=DP)
        elseif ( lp <= 36 ) then
           l = 6
           sig = CMPLX(0.0d0, -1.0d0, kind=DP)
        else
           l = 7
           sig = CMPLX(-1.0d0, 0.0d0, kind=DP)
        endif
        !sig=(0.d0,-1.d0)**(l-1)
        !if (ig == 1) print *,lp, ivl, jvl, ap (lp, ivl, jvl) , ylmk0 (ig, lp)
        work = qrad (i0, ijv, l, np) * uvx * wx + &
               qrad (i1, ijv, l, np) * pwx * vx - &
               qrad (i2, ijv, l, np) * pwx * ux + &
               qrad (i3, ijv, l, np) * px * uvx
        qg (ig) = qg (ig) + sig * CMPLX(ap (lp, ivl, jvl) * ylmk0 (ig, lp) * work, 0.d0, kind=DP)
     enddo
  enddo
  !$acc end kernels
  return

end subroutine qvan2_acc
