!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!

#ifdef _OPENACC
#define _CPUonly_ !!!
#define _GPUonly_
#else
#define _CPUonly_
#define _GPUonly_ !!!
#endif



!----------------------------------------------------------------------
SUBROUTINE addusdens(rho)
  !----------------------------------------------------------------------
  !
  USE realus,               ONLY : addusdens_r
  USE control_flags,        ONLY : tqr
  USE noncollin_module,     ONLY : nspin_mag
  USE fft_base,             ONLY : dfftp
  USE kinds,                ONLY : DP
  !
  IMPLICIT NONE
  !
  !
  REAL(kind=dp), INTENT(inout) :: rho(dfftp%nnr,nspin_mag)
  !
  IF ( tqr ) THEN
     CALL addusdens_r(rho)
  ELSE
     CALL addusdens_g(rho)
  ENDIF
  !
  RETURN
  !
END SUBROUTINE addusdens
!
!----------------------------------------------------------------------
SUBROUTINE addusdens_g(rho)
  !----------------------------------------------------------------------
  !
  !  This routine adds to the charge density the part which is due to
  !  the US augmentation.
  !
_GPUonly_  USE cublas,      ONLY : cublasDgemm
  USE kinds,                ONLY : DP
  USE ions_base,            ONLY : nat, ntyp => nsp, ityp
  USE fft_base,             ONLY : dfftp
  USE fft_interfaces,       ONLY : invfft
_GPUonly_  USE fft_interfaces,       ONLY : invfft_gpu
  USE gvect,                ONLY : ngm, gg, g, &
                                   eigts1, eigts2, eigts3, mill
  USE noncollin_module,     ONLY : noncolin, nspin_mag
  USE uspp,                 ONLY : becsum, okvan
  USE uspp_param,           ONLY : upf, lmaxq, nh
  USE control_flags,        ONLY : gamma_only
  USE wavefunctions_module, ONLY : psic
  USE mp_pools,             ONLY : inter_pool_comm
  USE mp,                   ONLY : mp_sum
  !
  IMPLICIT NONE
  !
  REAL(kind=dp), INTENT(inout) :: rho(dfftp%nnr,nspin_mag)
  !
  !     here the local variables
  !
  INTEGER :: ngm_s, ngm_e, ngm_l
  ! starting/ending indices, local number of G-vectors
  INTEGER :: ig, na, nt, ih, jh, ijh, is, nab, nb, nij
  ! counters

  REAL(DP), ALLOCATABLE :: tbecsum(:,:,:)
  ! \sum_kv <\psi_kv|\beta_l><beta_m|\psi_kv> for each species of atoms
  REAL(DP), ALLOCATABLE :: qmod (:), ylmk0 (:,:)
  ! modulus of G, spherical harmonics
  COMPLEX(DP), ALLOCATABLE :: skk(:,:), aux2(:,:)
  ! structure factors, US contribution to rho
  COMPLEX(DP), ALLOCATABLE ::  aux (:,:), qgm(:)
  ! work space for rho(G,nspin), Fourier transform of q
  !$acc declare create (aux (:,:)) device_resident(skk(:,:),aux2(:,:), tbecsum(:,:,:))
  
  INTEGER, ALLOCATABLE :: nlaux(:)
  IF (.not.okvan) RETURN
  !
  CALL start_clock ('addusdens')
  !
  ALLOCATE (aux (ngm, nspin_mag) )
  ! THIS IS A BUG OF PGI, CANNOT CREATE AND USE DATA FROM TYPE !
  ALLOCATE (nlaux, source=dfftp%nl)
  nlaux = dfftp%nl
  
  
  !
  !$acc kernels
  aux (:,:) = (0.d0, 0.d0)
  !$acc end kernels
  !
  ! With k-point parallelization, distribute G-vectors across processors
  ! ngm_s = index of first G-vector for this processor
  ! ngm_e = index of last  G-vector for this processor
  ! ngm_l = local number of G-vectors 
  !
  CALL divide (inter_pool_comm, ngm, ngm_s, ngm_e)
  ngm_l = ngm_e-ngm_s+1
  ! for the extraordinary unlikely case of more processors than G-vectors
  IF ( ngm_l <= 0 ) GO TO 10
  !
  ALLOCATE (qmod(ngm_l), qgm(ngm_l) )
  ALLOCATE (ylmk0(ngm_l, lmaxq * lmaxq) )
  !$acc enter data create (ylmk0 (:,:), qmod(:), qgm(:))
  CALL ylmr2 (lmaxq * lmaxq, ngm_l, g(1,ngm_s), gg(ngm_s), ylmk0, .true.)

  !$acc kernels present(gg, qmod)
  DO ig = 1, ngm_l
     qmod (ig) = sqrt (gg (ngm_s+ig-1) )
  ENDDO
  !$acc end kernels

  !
  DO nt = 1, ntyp
     IF ( upf(nt)%tvanp ) THEN
        !
        ! nij = max number of (ih,jh) pairs per atom type nt
        !
        nij = nh(nt)*(nh(nt)+1)/2
        !
        ! count max number of atoms of type nt
        !
        nab = 0
        DO na = 1, nat
           IF ( ityp(na) == nt ) nab = nab + 1
        ENDDO
        !
        ALLOCATE ( skk(ngm_l,nab), tbecsum(nij,nab,nspin_mag), aux2(ngm_l,nij) )
        !
        nb = 0
        DO na = 1, nat
           IF ( ityp(na) == nt ) THEN
              nb = nb + 1
              !$acc kernels present(becsum, tbecsum)
              DO is = 1, nspin_mag
                tbecsum(:,nb,is) = becsum(1:nij,na,is)
              END DO
              !$acc end kernels
_CPUonly_     !$omp parallel do default(shared)
_GPUonly_     !$acc parallel loop independent present(eigts1,eigts2,eigts3, mill, skk)
              DO ig = 1, ngm_l
                 skk(ig,nb) = eigts1 (mill (1,ngm_s+ig-1), na) * &
                              eigts2 (mill (2,ngm_s+ig-1), na) * &
                              eigts3 (mill (3,ngm_s+ig-1), na)
              ENDDO
_GPUonly_     !$acc end parallel
_CPUonly_     !$omp end parallel do
           ENDIF
        ENDDO
        !acc update device(tbecsum)

        DO is = 1, nspin_mag
           ! sum over atoms
           
_CPUonly_  CALL dgemm( 'N', 'T', 2*ngm_l, nij, nab, 1.0_dp, skk, 2*ngm_l,&
_CPUonly_            tbecsum(1,1,is), nij, 0.0_dp, aux2, 2*ngm_l )
           !$acc host_data use_device(skk,tbecsum,aux2)
_GPUonly_  CALL cublasDgemm( 'N', 'T', 2*ngm, nij, nab, 1.0_dp, skk, 2*ngm, &
_GPUonly_            tbecsum(1,1,is), nij, 0.0_dp, aux2, 2*ngm )
           !$acc end host_data
           !acc update host(aux2)
           ! sum over lm indices of Q_{lm}
           ijh = 0
           DO ih = 1, nh (nt)
              DO jh = ih, nh (nt)
                 ijh = ijh + 1
                 ! This call is duplicated becouse qvan2 inputs a REAL for a COMPLEX argument!
                 ! could easily go away if qgm becomes COMPLEX
_GPUonly_        CALL qvan2_acc (ngm_l, ih, jh, nt, qmod, qgm, ylmk0, .true.)
_CPUonly_        CALL qvan2     (ngm_l, ih, jh, nt, qmod, qgm, ylmk0, .false.)
_CPUonly_ !$omp parallel do default(shared) private(ig)
          !$acc kernels
                 DO ig = 1, ngm_l
                    aux(ngm_s+ig-1,is) = aux(ngm_s+ig-1,is)+aux2(ig,ijh)*qgm(ig)
                 ENDDO
          !$acc end kernels
_CPUonly_ !$omp end parallel do
             ENDDO
           ENDDO
        ENDDO
        DEALLOCATE (aux2, tbecsum, skk )
     ENDIF
  ENDDO
  !
  !$acc exit data delete (ylmk0 (:,:), qmod(:), qgm(:))
  DEALLOCATE (ylmk0)
  DEALLOCATE (qgm, qmod)
  !
  10 CONTINUE
  !
  !     convert aux to real space and add to the charge density
  !
  ! USE CUDA MPI!
  !$acc update host(aux (:,:))
  CALL mp_sum( aux, inter_pool_comm )
  !$acc update device(aux (:,:))
  !
#ifdef DEBUG_ADDUSDENS
  CALL start_clock ('addus:fft')
#endif
  
  !$acc data present(rho) copyin(nlaux)
  
  DO is = 1, nspin_mag
     !$acc kernels 
     psic(:) = (0.d0, 0.d0)
     do ig = lbound(nlaux,1),ubound(nlaux,1)
      psic( nlaux(ig) ) = aux(ig,is)
     end do
     !IF (gamma_only) psic( dfftp%nlm(:) ) = CONJG (aux(:,is))
     !$acc end kernels

     !$acc host_data use_device(psic)
     CALL invfft_gpu ('Dense', psic, dfftp)
     !$acc end host_data

     !$acc kernels
     rho(:, is) = rho(:, is) +  DBLE (psic (:) )
     !$acc end kernels
  ENDDO
  !$acc end data
#ifdef DEBUG_ADDUSDENS
  CALL stop_clock ('addus:fft')
#endif
  DEALLOCATE (aux)
  DEALLOCATE (nlaux)

  CALL stop_clock ('addusdens')
  RETURN
END SUBROUTINE addusdens_g

