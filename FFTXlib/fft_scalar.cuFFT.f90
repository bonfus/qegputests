!
! Copyright (C) Quantum ESPRESSO group
!
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .

!=----------------------------------------------------------------------=!
   MODULE fft_scalar_cuFFT
!=----------------------------------------------------------------------=!
#ifdef USE_CUDA
       USE fft_param
!! iso_c_binding provides C_PTR, C_NULL_PTR, C_ASSOCIATED
       USE iso_c_binding

       USE cudafor
       USE cufft
      
       IMPLICIT NONE
       SAVE
       PRIVATE
       PUBLIC :: cfft3d_gpu, cfft3ds_gpu

!=----------------------------------------------------------------------=!
   CONTAINS
!=----------------------------------------------------------------------=!


!
!=----------------------------------------------------------------------=!
!
!
!
!         3D scalar FFTs
!
!
!
!=----------------------------------------------------------------------=!
!

#ifdef USE_CUDA

   SUBROUTINE cfft3d_gpu( f, nx, ny, nz, ldx, ldy, ldz, howmany, isign )

  !     driver routine for 3d complex fft of lengths nx, ny, nz
  !     input  :  f(ldx*ldy*ldz)  complex, transform is in-place
  !     ldx >= nx, ldy >= ny, ldz >= nz are the physical dimensions
  !     of the equivalent 3d array: f3d(ldx,ldy,ldz)
  !     (ldx>nx, ldy>ny, ldz>nz may be used on some architectures
  !      to reduce memory conflicts - not implemented for FFTW)
  !     isign > 0 : f(G) => f(R)   ; isign < 0 : f(R) => f(G)
  !
  !     Up to "ndims" initializations (for different combinations of input
  !     parameters nx,ny,nz) are stored and re-used if available

     IMPLICIT NONE

     INTEGER, INTENT(IN) :: nx, ny, nz, ldx, ldy, ldz, howmany, isign
     COMPLEX (DP), device :: f(:)
     INTEGER :: i, k, j, err, idir, ip, istat
     REAL(DP) :: tscale
     INTEGER, SAVE :: icurrent = 1
     INTEGER, SAVE :: dims(3,ndims) = -1

!     C_POINTER, save :: fw_plan(ndims) = 0
!     C_POINTER, save :: bw_plan(ndims) = 0
     INTEGER, SAVE :: cufft_plan_3d( ndims ) = 0


     IF ( nx < 1 ) &
         call fftx_error__('cfft3d',' nx is less than 1 ', 1)
     IF ( ny < 1 ) &
         call fftx_error__('cfft3d',' ny is less than 1 ', 1)
     IF ( nz < 1 ) &
         call fftx_error__('cfft3',' nz is less than 1 ', 1)

     !
     !   Here initialize table only if necessary
     !
     CALL lookup()

     IF( ip == -1 ) THEN

       !   no table exist for these parameters
       !   initialize a new one

       CALL init_plan()

     END IF

     !
     !   Now perform the 3D FFT using the machine specific driver
     !

     IF( isign < 0 ) THEN

        istat = cufftExecZ2Z( cufft_plan_3d(ip), f(1), f(1), CUFFT_FORWARD )

       tscale = 1.0_DP / DBLE( nx * ny * nz )
!$cuf kernel do(1) <<<*,*>>>
        DO i=1, nx*ny*nz
           f( i ) = f( i ) * tscale
        END DO
!       call ZDSCAL( nx * ny * nz, tscale, f(1), 1)

     ELSE IF( isign > 0 ) THEN

!       call FFTW_INPLACE_DRV_3D( bw_plan(ip), 1, f(1), 1, 1 )
        istat = cufftExecZ2Z( cufft_plan_3d(ip), f(1), f(1), CUFFT_INVERSE )

     END IF

     RETURN

   CONTAINS 

     SUBROUTINE lookup()
     ip = -1
     DO i = 1, ndims
       !   first check if there is already a table initialized
       !   for this combination of parameters
       IF ( ( nx == dims(1,i) ) .and. &
            ( ny == dims(2,i) ) .and. &
            ( nz == dims(3,i) ) ) THEN
         ip = i
         EXIT
       END IF
     END DO
     END SUBROUTINE lookup

     SUBROUTINE init_plan()
       INTEGER, PARAMETER :: RANK=3
       INTEGER :: FFT_DIM(RANK), DATA_DIM(RANK)
       INTEGER :: STRIDE, DIST, BATCH

        FFT_DIM(1) = nz
        FFT_DIM(2) = ny
        FFT_DIM(3) = nx
       DATA_DIM(1) = ldz
       DATA_DIM(2) = ldy
       DATA_DIM(3) = ldx
            STRIDE = 1
              DIST = ldx*ldy*ldz
             BATCH = 1

       IF( cufft_plan_3d( icurrent) /= 0 )  istat = cufftDestroy( cufft_plan_3d(icurrent) )

       istat = cufftPlanMany( cufft_plan_3d( icurrent), RANK, FFT_DIM, &
                              DATA_DIM, STRIDE, DIST, &
                              DATA_DIM, STRIDE, DIST, &
                              CUFFT_Z2Z, BATCH )

       !IF ( nx /= ldx .or. ny /= ldy .or. nz /= ldz ) &
       !  call fftx_error__('cfft3','not implemented',1)
       !IF( fw_plan(icurrent) /= 0 ) CALL DESTROY_PLAN_3D( fw_plan(icurrent) )
       !IF( bw_plan(icurrent) /= 0 ) CALL DESTROY_PLAN_3D( bw_plan(icurrent) )
       !idir = -1; CALL CREATE_PLAN_3D( fw_plan(icurrent), nx, ny, nz, idir)
       !idir =  1; CALL CREATE_PLAN_3D( bw_plan(icurrent), nx, ny, nz, idir)
       dims(1,icurrent) = nx; dims(2,icurrent) = ny; dims(3,icurrent) = nz
       ip = icurrent
       icurrent = MOD( icurrent, ndims ) + 1
     END SUBROUTINE init_plan

   END SUBROUTINE cfft3d_gpu
   
   SUBROUTINE cfft3ds_gpu (f, nx, ny, nz, ldx, ldy, ldz, howmany, isign, &
     do_fft_z, do_fft_y)
     !
     !     driver routine for 3d complex "reduced" fft - see cfft3d
     !     The 3D fft are computed only on lines and planes which have
     !     non zero elements. These lines and planes are defined by
     !     the two integer vectors do_fft_y(nx) and do_fft_z(ldx*ldy)
     !     (1 = perform fft, 0 = do not perform fft)
     !     This routine is implemented only for fftw, essl, acml
     !     If not implemented, cfft3d is called instead
     !
     !----------------------------------------------------------------------
     !
     implicit none
         INTEGER, PARAMETER  :: stdout = 6
     
     integer :: nx, ny, nz, ldx, ldy, ldz, howmany, isign
     !
     !   logical dimensions of the fft
     !   physical dimensions of the f array
     !   sign of the transformation
     
     complex(DP),device :: f ( ldx * ldy * ldz )
     integer :: do_fft_y(:), do_fft_z(:)
     !
     integer :: m, incx1, incx2
     INTEGER :: i, k, j, err, idir, ip,  ii, jj, istat
     REAL(DP) :: tscale
     INTEGER, SAVE :: icurrent = 1
     INTEGER, SAVE :: dims(3,ndims) = -1
     
     !TYPE(C_PTR), SAVE :: fw_plan ( 3, ndims ) = C_NULL_PTR
     !TYPE(C_PTR), SAVE :: bw_plan ( 3, ndims ) = C_NULL_PTR
     INTEGER, SAVE :: cufft_plan_1d( 3, ndims ) = 0
     !CALL cfft3d_gpu (f, nx, ny, nz, ldx, ldy, ldz, howmany, isign)
     !return
     tscale = 1.0_DP
     
     IF( ny /= ldy ) &
       CALL fftx_error__(' cfft3ds ', ' wrong dimensions: ny /= ldy ', 1 )
     IF( howmany < 1 ) &
       CALL fftx_error__(' cfft3ds ', ' howmany less than one ', 1 )
     
     CALL lookup()
     
     IF( ip == -1 ) THEN
     
       !   no table exist for these parameters
       !   initialize a new one
     
       CALL init_plan()
     
     END IF
     
     
     IF ( isign > 0 ) THEN
     
        !
        !  k-direction ...
        !
     
        !incx1 = ldx * ldy;  incx2 = 1;  m = 1
     
        do i =1, nx
           do j =1, ny
              ii = i + ldx * (j-1)
              if ( do_fft_z(ii) > 0) then
                 ! call dfftw_execute_dft( bw_plan( 3, ip), f( ii:), f( ii:) )
                 istat = cufftExecZ2Z( cufft_plan_1d(3, ip), f( ii:), f( ii:), CUFFT_FORWARD )
              end if
           end do
        end do
     
        !
        !  ... j-direction ...
        !
     
        !incx1 = ldx;  incx2 = ldx*ldy;  m = nz
     
        do i = 1, nx
           if ( do_fft_y( i ) == 1 ) then
             !call dfftw_execute_dft( bw_plan( 2, ip), f( i: ), f( i: ) )
             istat = cufftExecZ2Z( cufft_plan_1d(2, ip), f(i:), f( i:), CUFFT_FORWARD )
           endif
        enddo
     
        !
        !  ... i - direction
        !
     
        !incx1 = 1;  incx2 = ldx;  m = ldy*nz
     
        !call dfftw_execute_dft( bw_plan( 1, ip), f( 1: ), f( 1: ) )
        istat = cufftExecZ2Z( cufft_plan_1d(1, ip), f( 1:), f( 1:), CUFFT_FORWARD )
     
     ELSE
     
        !
        !  i - direction ...
        !
     
        !incx1 = 1;  incx2 = ldx;  m = ldy*nz
     
        !call dfftw_execute_dft( fw_plan( 1, ip), f( 1: ), f( 1: ) )
        istat = cufftExecZ2Z( cufft_plan_1d(1, ip), f( 1:), f( 1:), CUFFT_INVERSE )
     
        !
        !  ... j-direction ...
        !
     
        !incx1 = ldx;  incx2 = ldx*ldy;  m = nz
     
        do i = 1, nx
           if ( do_fft_y ( i ) == 1 ) then
             !call dfftw_execute_dft( fw_plan( 2, ip), f( i: ), f( i: ) )
             istat = cufftExecZ2Z( cufft_plan_1d(2, ip), f( i:), f( i:), CUFFT_INVERSE )
           endif
        enddo
     
        !
        !  ... k-direction
        !
     
        !incx1 = ldx * ny;  incx2 = 1;  m = 1
     
        do i = 1, nx
           do j = 1, ny
              ii = i + ldx * (j-1)
              if ( do_fft_z ( ii) > 0) then
                 !call dfftw_execute_dft( fw_plan( 3, ip), f(ii:), f(ii:) )
                 istat = cufftExecZ2Z( cufft_plan_1d(3, ip), f( ii:), f( ii:), CUFFT_INVERSE )
              end if
           end do
        end do
     
        !call DSCAL (2 * ldx * ldy * nz, 1.0_DP/(nx * ny * nz), f(1), 1)
        tscale = 1.0_DP / DBLE( nx * ny * nz )
        !$cuf kernel do(1) <<<*,*>>>
        DO i=1, nx*ny*nz
           f( i ) = f( i ) * tscale
        END DO
     
     END IF
     RETURN
     
      CONTAINS
     
        SUBROUTINE lookup()
        ip = -1
        DO i = 1, ndims
          !   first check if there is already a table initialized
          !   for this combination of parameters
          IF( ( nx == dims(1,i) ) .and. ( ny == dims(2,i) ) .and. &
              ( nz == dims(3,i) ) ) THEN
            ip = i
            EXIT
          END IF
        END DO
        END SUBROUTINE lookup
     
        SUBROUTINE init_plan()
           INTEGER, PARAMETER :: RANK=3
           INTEGER :: FFT_DIM(RANK), HOW_MANY(RANK), DATA_DIM(RANK)
           INTEGER :: STRIDE(RANK), DIST(RANK)
           INTEGER :: i
           
            FFT_DIM(1) = nx
            FFT_DIM(2) = ny
            FFT_DIM(3) = nz
           HOW_MANY(1) = ny*nz
           HOW_MANY(2) = nz
           HOW_MANY(3) = 1
           DATA_DIM(1) = ldx
           DATA_DIM(2) = ldy
           DATA_DIM(3) = ldz
             STRIDE(1) = 1
             STRIDE(2) = ldx
             STRIDE(3) = ldx*ldy
               DIST(1) = ldx
               DIST(2) = ldx*ldy
               DIST(3) = 1
        
          IF( cufft_plan_1d( 1, icurrent) /= 0 )  istat = cufftDestroy( cufft_plan_1d(1, icurrent) )
          IF( cufft_plan_1d( 2, icurrent) /= 0 )  istat = cufftDestroy( cufft_plan_1d(2, icurrent) )
          IF( cufft_plan_1d( 3, icurrent) /= 0 )  istat = cufftDestroy( cufft_plan_1d(3, icurrent) )

       
       !CALL dfftw_plan_many_dft( fw_plan( 1, icurrent), &
       !     1, nx, ny*nz, f(1:), (/ldx, ldy, ldz/), 1, ldx, &
       !                   f(1:), (/ldx, ldy, ldz/), 1, ldx, idir, FFTW_ESTIMATE)
       !CALL dfftw_plan_many_dft( fw_plan( 2, icurrent), &
       !     1, ny, nz, f(1:), (/ldx, ldy, ldz/), ldx, ldx*ldy, &
       !                f(1:), (/ldx, ldy, ldz/), ldx, ldx*ldy, idir, FFTW_ESTIMATE)
       !CALL dfftw_plan_many_dft( fw_plan( 3, icurrent), &
       !     1, nz, 1, f(1:), (/ldx, ldy, ldz/), ldx*ldy, 1, &
       !               f(1:), (/ldx, ldy, ldz/), ldx*ldy, 1, idir, FFTW_ESTIMATE)
          
          
          DO i = 1,3 
            istat = cufftPlanMany( cufft_plan_1d( i, icurrent), 1, FFT_DIM(i), &
                              DATA_DIM(i), STRIDE(i), DIST(i), &
                              DATA_DIM(i), STRIDE(i), DIST(i), &
                              CUFFT_Z2Z, HOW_MANY(i) )
            ! set current dimension as a lookup parameter along the three directions
            dims(1,icurrent) = FFT_DIM(i)
          END DO
          ! dims(1,icurrent) = nx; dims(2,icurrent) = ny; dims(3,icurrent) = nz
          
          ip = icurrent
          icurrent = MOD( icurrent, ndims ) + 1
        END SUBROUTINE init_plan
     
     END SUBROUTINE cfft3ds_gpu
#endif
#endif
!=----------------------------------------------------------------------=!
 END MODULE fft_scalar_cuFFT
!=----------------------------------------------------------------------=!


